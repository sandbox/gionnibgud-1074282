<?php
// $Id: jquery_clock.drush.inc,v 1.1.2.1 2010/10/29 17:45:40 rgeorgy Exp $

/**
 * @file
 *   drush integration for jquery_clock.
 */

/**
 * Implementation of hook_drush_command().
 *
 * In this hook, you specify which commands your
 * drush module makes available, what it does and
 * description.
 *
 * Notice how this structure closely resembles how
 * you define menu hooks.
 *
 * @See drush_parse_command() for a list of recognized keys.
 *
 * @return
 *   An associative array describing your command(s).
 */
function jquery_clock_drush_command() {
  $items = array();

  $items['dl-jqclock'] = array(
    'callback' => 'drush_jquery_clock_jqclock_download',
    'description' => dt('Downloads the required jquery clock jquery plugin.'),
  );
  return $items;
}

/**
 * Implementation of hook_drush_help().
 *
 * This function is called whenever a drush user calls
 * 'drush help <name-of-your-command>'
 *
 * @param
 *   A string with the help section (prepend with 'drush:')
 *
 * @return
 *   A string with the help text for your command.
 */
function jquery_clock_drush_help($section) {
  switch ($section) {
    case 'drush:dl-jqclock':
      return dt("Downloads the required jquery clock jquery plugin.");
  }
}

/**
 * Example drush command callback.
 *
 * This is where the action takes place.
 *
 * In this function, all of Drupals API is (usually) available, including
 * any functions you have added in your own modules/themes.
 *
 * To print something to the terminal window, use drush_print().
 *
 */
function drush_jquery_clock_jqclock_download() {
  if(module_exists('libraries')) {
    $path = 'sites/all/libraries/jquery_clock';

    // Create the path if it does not exist.
    if (!is_dir($path)) {
      drush_op('mkdir', $path);
      drush_log(dt('Directory @path was created', array('@path' => $path)), 'notice');
    }
  }
  else {
    $path = drupal_get_path('module', 'jquery_clock') . '/';
  }
  drush_op('chdir', $path);
  // Download the plugin.
  if (drush_shell_exec('wget http://plugins.jquery.com/files/jqClock.min_.js__0.txt')) {
    //http://github.com/Lwangaman/jQuery-Clock-Plugin/blob/master/jqClock.min.js
    drush_shell_exec('mv jqClock.min*.txt jqClock.min.js');
    drush_log(dt('jqClock.min.js has been downloaded to @path', array('@path' => $path)), 'success');
  }
  else {
    drush_log(dt('Drush was unable to download jqClock.min.js to @path', array('@path' => $path)), 'error');
  }
}
